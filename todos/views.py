from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import ListView
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# class TodoListView(ListView):
#    model = TodoList
#    template_name = "todo_list.html"
#    context_object_name = "todo_lists"


# Create your views here.
def todo_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_list": todo_lists
    }  # todo_list is object can label as object
    return render(request, "todos/todo_list.html", context)


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list": todo_list,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_vaild():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm()
    return render(request, "todos/create.html", {"form": form})


def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_vaild():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm(instance=todo_list)
    return render(request, "todos/update.html", {"form": form})


def todo_list_delete(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html", {"todo_list": todo_list})


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_vaild():
            todo_item = form.save()
            return redirect("todo_list_detail", id=todo_item.id)
    else:
        form = TodoItemForm()
    return render(request, "todos/item_create.html", {"form": form})


def todo_item_update(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_vaild():
            form.save()
            return redirect("todo_list_detail", id=todo_item.todo_list.id)
    else:
        form = TodoItemForm(instance=todo_item)
    return render(
        request,
        "todos/item_update.html",
        {"form": form, "todo_item": todo_item},
    )
